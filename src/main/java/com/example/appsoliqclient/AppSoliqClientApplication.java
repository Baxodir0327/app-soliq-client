package com.example.appsoliqclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppSoliqClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppSoliqClientApplication.class, args);
    }

}
